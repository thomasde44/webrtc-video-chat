Multi-Client WebRTC Video
=========================

Based on https://github.com/shanet/WebRTC-Example

## Usage

The signaling server uses Node.js and `ws` and can be started as 
follows:

```
$ npm install
$ npm start
```


